
		var bonnesReponses = 0;
		var mauvaisesReponses = 0;
        // Liste de mots invariables
        const motsInvariables = ['toujours', 'jamais', 'partout', 'souvent', 'aussi','ailleurs',
'à cause de',
'à peine',
'à travers',
'afin',
'ailleurs',
'ainsi',
'alors',
'après',
'assez',
'au-dessous',
'au-dessus',
"aujourd'hui",
'auprès',
'aussi',
'aussitôt',
'autant',
'autour',
'autrefois',
'autrement',
'avant',
'avec',
'beaucoup',
'bien',
'bientôt',
'car',
'ceci',
'cela',
'cependant',
'chez',
'combien',
'comment',
'comment',
"d'abord",
"d'habitude",
'dans',
'davantage',
'debout',
'dedans',
'dehors',
'déjà',
'demain',
'depuis',
'derrière',
'dès que',
'désormais',
'dessous',
'dessus',
'devant',
'donc',
'dont',
'durant',
'encore',
'enfin',
'ensemble',
'ensuite',
'entre',
'envers',
'exprès',
'finalement',
'guère',
'habituellement',
'hélàs',
'hier',
'hors',
'ici',
'jamais',
'là-bas',
'loin',
'longtemps',
'lorsque',
'mainteant',
'mais',
'malgré',
'mieux',
'moins',
'naturrelement',
'ne ... pas',
'néanmoins',
'non',
'oui',
'parce que',
'parfois',
'parmi',
'pendant',
'partout',
'peu',
'peut-être',
'plus',
'plusieurs',
'plutôt',
'pour',
'pourquoi',
'pourtant',
'près',
'presque',
'puis',
'quand',
'quelque chose',
'quelquefois',
"qu'est-ce que",
"qu'est-ce qui",
'quoi',
'rapidement',
'rien',
'sans',
'sauf',
'selon',
'seulement',
'sinon',
'sitôt',
'soudain',
'sous',
'souvent',
'sur',
'sûrement',
'surtout',
'tandis que',
'tant',
'tant pis',
'tantôt',
'tard',
'tellement',
'tôt',
'toujours',
'tout',
'tout à coup',
'tout à fait',
'toutefois',
'très',
'trop',
'vers',
'vite',
'voici',
'voilà',
'volontier',
'vriament'
];

        // Variables de jeu
        let score = 0;

        // Fonction pour choisir une nouvelle question
        function nouvelleQuestion() {
            const index = Math.floor(Math.random() * motsInvariables.length);
			
			mot = motsInvariables[index];
			
			if(mot.length>level+3){
				return mot;
			}else{
				while(mot.length<level+3){
					index = Math.floor(Math.random() * motsInvariables.length);
					mot = motsInvariables[index];
				}
				return mot; 
			}
        }

        // Fonction pour masquer certaines lettres du mot
        function masquerLettres(mot, nombreLettresAMasquer) {
            const motArray = mot.split('');
            const indicesAMasquer = [];

            while (indicesAMasquer.length < nombreLettresAMasquer) {
                const indice = Math.floor(Math.random() * motArray.length);
                if (!indicesAMasquer.includes(indice)) {
                    indicesAMasquer.push(indice);
                }
            }

            for (let i = 0; i < indicesAMasquer.length; i++) {
                motArray[indicesAMasquer[i]] = '_';
            }

            return motArray.join('');
        }

        // Fonction pour vérifier la réponse
        function checkAnswer() {
			if (event.keyCode === 13) {
				const userInput = document.getElementById('userInput').value.toLowerCase();
				const currentWord = document.getElementById('result');
				const scoreElement = document.getElementById('scoreValue');

				const motActuel = document.getElementById('instructions').getAttribute('data-mot');

				if (userInput === motActuel) {
					currentWord.textContent = 'Correct !';
					currentWord.style.fontWeight = 'bold';
					currentWord.style.fontSize = '20px';
					currentWord.style.color = 'green';
					bonnesReponses++;
				} else {
					currentWord.style.fontWeight = 'bold';
					currentWord.style.fontSize = '20px';
					currentWord.style.color = 'red';
					currentWord.textContent = 'Incorrect. La réponse correcte est ' + motActuel + '.';
					mauvaisesReponses++;
				}

				if ((bonnesReponses + mauvaisesReponses) === 10) {
					if (bonnesReponses > 6) {
						
						if(level === 1){
							var player_video = document.getElementById('player');
							player_video.style.display = 'block';
							loadRandomVideo();
							}
						}
						clearInterval(chronoInterval);
						document.getElementById('RAZ').style.display = 'block';
						document.getElementById('instructions').style.display = 'none';
						document.getElementById('userInput').value = '';
						document.getElementById('userInput').style.display = 'none';
						document.getElementById('result').style.color = 'green';
						document.getElementById('result').style.fontSize = '24px';
						
						setInterval(blink, 500);
						
						document.getElementById('result').style.fontWeight = 'bold';
						document.getElementById('result').textContent = 'Vous avez ' + bonnesReponses + ' bonnes réponses sur ' + (bonnesReponses + mauvaisesReponses) + ' en ' + tempsPasse + ' secondes';
					}else{
						// Affiche une nouvelle question
						setTimeout(function () {
							document.getElementById('userInput').value = '';
							currentWord.textContent = '';

							const newWord = nouvelleQuestion();
							const motMasque = masquerLettres(newWord, level); // Changer 2 à la quantité désirée
							document.getElementById('instructions').textContent = `Trouve le mot invariable : "${motMasque}"`;
							document.getElementById('instructions').setAttribute('data-mot', newWord);
						}, 1000); // Attendez 1 seconde avant de changer de question
				}
			}
        }

		function blink() {
			if (document.getElementById('result').style.color === 'green') {
				document.getElementById('result').style.color = ''; // Rétablir la couleur par défaut (noire ou autre)
			} else {
				document.getElementById('result').style.color = 'green'; // Changer la couleur du texte en rouge
			}
		}
		
		function init() {


			replay();
		}
		
		function replay() {
			var answer = 0;
			bonnesReponses = 0;
			mauvaisesReponses = 0;
			
			if(level == undefined)
				level = 1;
			else
				level = level+1;
            const initialWord = nouvelleQuestion();
            const motMasque = masquerLettres(initialWord, level); // Changer 2 à la quantité désirée
			
			document.getElementById('niveau').textContent = `Vous êtes au niveau ${level}`
            document.getElementById('instructions').textContent = `Trouve le mot invariable : "${motMasque}"`;
            document.getElementById('instructions').setAttribute('data-mot', initialWord);
			document.getElementById('chrono').style.display = 'block';
			
			document.getElementById('player').style.display = 'none';
			document.getElementById('RAZ').style.display = 'none';
			document.getElementById('result').textContent = '';
			document.getElementById('instructions').style.display = 'block';
			document.getElementById('userInput').style.display = 'block';
			document.getElementById('begin').style.display = 'none';
			clearInterval(chronoInterval);
			tempsPasse = 0; // Temps écoulé en secondes
			chronoInterval = 0;
			document.getElementById('tempsPasse').textContent = tempsPasse; 
			demarrerChrono();
		}
		
		function demarrerChrono() {
			chronoInterval = setInterval(function() {
				tempsPasse++;
				document.getElementById('tempsPasse').textContent = tempsPasse;
			}, 1000); // Mettre à jour le chronomètre toutes les 1000 ms (1 seconde)
		}
		
		function loadRandomVideo() {
			// Array de vidéos YouTube d'environ 1 minute (ajoutez vos liens ici)
			/*
				'v=yws-sP342Ik?si=Y-4W0-121f6XBhTx',
				'v=DqNaPewELqc?si=DPdzL-3nvhEl_3_j',
				'v=DZXWZq-MfSU?si=pDb4CUPzm57hikI9',
				'v=L3qJFjlDvKY?t=35?si=CSx1xDAjsAnEZiID',
				'v=9B_e_XJyHmU?si=gwPuTVH2YZJgFBfb',
				'v=5Q6n5NE6Cb4?si=AVSrr9Dzc4ie0h8c',
				'v=cR7o655tVw8?si=unt45V1XYwhj8PIc',
				'v=32n1ld2KSdA?si=tLeaoWmLoyysM1ML',
				'v=geLsdbzDwGE?si=9vvwM2dZ7_9fgwZX',*/
			switch(localStorage.getItem('profil')){
				case '1':
				default:
					var videoLinks = [
						'8dWD1WzSZ5o?si=4CRyfYkC89L1HkZ8'
					];

					// Choisir un lien vidéo aléatoire
					var randomIndex = Math.floor(Math.random() * videoLinks.length);
					var randomVideoLink = videoLinks[randomIndex];

					// Extraire l'ID de la vidéo depuis le lien
					var videoId = randomVideoLink.split('v=')[1];

					// Construire l'URL de l'iframe
					var iframeUrl = 'https://www.youtube.com/embed/' + videoId + '?apikey=AIzaSyDc9EV4pA80VOONaSp9NsGWONSEpdktIHo&enablejsapi=1&autoplay=1';
					
					// Créer l'élément iframe pour la vidé
					var playerDiv = document.getElementById('player');
					playerDiv.innerHTML = '<iframe width="560" height="315" src="' + iframeUrl + '" frameborder="0" allowfullscreen></iframe>';
					break;
				case '2':
					var videoLinks = [
						'v=1NhppWbuwbB6W1FIrApPOC72gGAKnoL2Q'
					];
					
					var randomIndex = Math.floor(Math.random() * videoLinks.length);
					var randomVideoLink = videoLinks[randomIndex];

					// Extraire l'ID de la vidéo depuis le lien
					var videoId = randomVideoLink.split('v=')[1];
					var iframeUrl = 'https://drive.google.com/file/d/' + videoId + '/preview';

					var playerDiv = document.getElementById('player');
					playerDiv.innerHTML = '<iframe src="' + iframeUrl + '" allow="autoplay"></iframe>';
					break;
				}
			}
// Appelez demarrerChrono() au chargement de la page pour démarrer le chronomètre
window.onload = demarrerChrono;		