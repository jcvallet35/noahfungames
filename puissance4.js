let currentPlayer;
let isGameOver;
let isComputerTurn;
let colPlayer1;
let player1 = 'Noah';
let player2 = 'PC';
let victoirePlayer1=0;
let victoirePlayer2=0;
let passage = 1;
const cols = 7;
const rows = 7;

function initializeGame(nbPas){
    isGameOver = false;
    isComputerTurn = false;

    let randomNbAlea = Math.floor(Math.random() * 2) + 1;
    
    currentPlayer = player1;
    if(randomNbAlea === 2){
        currentPlayer = player2;
        setTimeout(makeComputerMove, 1000);
    }

    const level = getElementById('level');
    if(level!=undefined){
        level.textContent = `Level ${nbPas} - Le premier à jouer est ${currentPlayer}`;
        level.style.fontWeight = 'bold';
        level.style.fontSize = '20px';
        level.style.color = 'Yellow';
    }

    const gameBoard = getElementById("game-board");
    if(gameBoard!=null){
        gameBoard.innerHTML = '';
    }

    // Create the game board
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            const cell = document.createElement("div");
            cell.classList.add("cell");
            cell.dataset.row = i;
            cell.dataset.col = j;
            cell.addEventListener("click", handleCellClick);
            if(gameBoard!=null){
                gameBoard.appendChild(cell);
            }
        }
    }

    function handleCellClick(event) {
        if (isGameOver || (isComputerTurn && currentPlayer === player2)) return;

        const clickedCell = event.target;
        const row = parseInt(clickedCell.dataset.row);
        const col = parseInt(clickedCell.dataset.col);

        // Check if the column is full
        for (let i = rows - 1; i >= 0; i--) {
            const cell = querySelector(i,col);
            if (cell!=null && !cell.title) {
                cell.title = currentPlayer;
				const image = document.createElement("img");
				if(currentPlayer === player1){
					image.src = "./img/rond_rouge.png";
					image.alt = player1;
				}else{
					image.src = "./img/rond_vert.png"
					image.alt = player2;
				}
				cell.appendChild(image);
                if (checkForWin(i, col)) {
                    gereMsgAccueil(`Player ${currentPlayer} wins!`,'red');
                    victoirePlayer1+=1;
                    if(victoirePlayer1-victoirePlayer2>5){
                        loadRandomVideo();
                    }
                    isGameOver = true;
                } else if (checkForDraw()) {
                        (`Match null`, 'red');
                    isGameOver = true;
                } else {
                    currentPlayer = getSwitchPlayer(player1, player2);

                    if(currentPlayer === player2){
                        isComputerTurn = true;
                        setTimeout(makeComputerMove, 1000);
                    }else{
                        isComputerTurn = false;
                    }
                }

                break;
            }
        }
    }

    // Switch player
    function getSwitchPlayer(cond1,cond2){
        return currentPlayer === cond1 ? cond2 : cond1;
    }

    function gereMsgAccueil(msg, color){
        getElementById('result').textContent = msg;
        getElementById('result').style.fontWeight = 'bold';
        getElementById('result').style.fontSize = '20px';
        getElementById('result').style.color = color;
        getElementById('RAZ').style.display = 'block';
    }

    function makeComputerMove(){
        let availableColumns = [];

        const bestCol = chooseBestColumn();
        colPlayer1 = bestCol;

        const bestColForWin = chooseBestColumnForWin();
        if(bestColForWin.length>0){
            colPlayer1 = bestColForWin[0][0];
        }

        for(let col = 0; col < cols; col++){
            const topCell = querySelectorCol(col);
            if(topCell!=undefined && !topCell.title){
                availableColumns.push(col);
            }
        }

        if(availableColumns.length>0){
            let randomColumn;
            if(colPlayer1!=undefined){
                randomColumn = colPlayer1;
                colPlayer1=undefined;
            }else{
                randomColumn = availableColumns[Math.floor(Math.random() * (availableColumns.length))];
                while(randomColumn > 6){
                    randomColumn = availableColumns[Math.floor(Math.random() * (availableColumns.length))];
                }
            }
            for (let i = rows - 1; i >= 0; i--) {
                const cell = querySelector(i,randomColumn); 
                if (!cell.title) {
                    cell.title = player2;
                    const image = document.createElement("img");
                    if(currentPlayer === player1){
                        image.src = "./img/rond_rouge.png";
                        image.alt = player1;
                    }else{
                        image.src = "./img/rond_vert.png"
                        image.alt = player2;
                    }
                    cell.appendChild(image);
                    const row = parseInt(cell.dataset.row); 
                    const col = parseInt(cell.dataset.col); 
                    if (checkForWin(row, col)) { 
                        gereMsgAccueil(`Player ${currentPlayer} wins!`, 'green');
                        victoirePlayer2+=1;
                        isGameOver = true; 
                    } else if (checkForDraw()) { 
                        isGameOver = true; 
                    } else { 
                        currentPlayer = getSwitchPlayer(player2, player1);
                    }
                    break;
                }else if(i===0){
                    i = rows;
                    randomColumn = 7;
                    while(randomColumn > 6){
                        randomColumn = availableColumns[Math.floor(Math.random() * (availableColumns.length))];
                    }
                }
            }
        }
    }

    function checkForDraw() {
        for (let col = 0; col < cols; col++) {
            const topCell = querySelectorCol(col);
            if (!topCell.title) {
                return false;
            }
        }
        return true;
    }

    function loadRandomVideo() {
        var player_video = getElementById('player');
        player_video.style.display = 'block';

        switch(localStorage.getItem('profil')){
            case '1':
            default:
                var videoLinks = [
                    'v=J3f_sffNXe0?si=B5lbrosAXGXB3R4P',
                    'v=cVJFuCiY3YU?si=81rU2M_qQ433s65G',
                    'v=TwLEHTEajtg?si=yWkzk40mCWMVkDvG'
                ];
    
                // Choisir un lien vidéo aléatoire
                var randomIndex = Math.floor(Math.random() * videoLinks.length);
                var randomVideoLink = videoLinks[randomIndex];
    
                // Extraire l'ID de la vidéo depuis le lien
                var videoId = randomVideoLink.split('v=')[1];
    
                // Construire l'URL de l'iframe
                var iframeUrl = 'https://www.youtube.com/embed/' + videoId + '?apikey=AIzaSyDc9EV4pA80VOONaSp9NsGWONSEpdktIHo&enablejsapi=1&autoplay=1';
                
                // Créer l'élément iframe pour la vidé
                var playerDiv = getElementById('player');
                playerDiv.innerHTML = '<iframe width="560" height="315" src="' + iframeUrl + '" frameborder="0" allowfullscreen></iframe>';
                break;
            case '2':
                var videoLinks = [
                    'v=1NhppWbuwbB6W1FIrApPOC72gGAKnoL2Q'
                ];
                
                var randomIndex = Math.floor(Math.random() * videoLinks.length);
                var randomVideoLink = videoLinks[randomIndex];
    
                // Extraire l'ID de la vidéo depuis le lien
                var videoId = randomVideoLink.split('v=')[1];
                var iframeUrl = 'https://drive.google.com/file/d/' + videoId + '/preview';
    
                var playerDiv = getElementById('player');
                playerDiv.innerHTML = '<iframe src="' + iframeUrl + '" allow="autoplay"></iframe>';
                break;
            }
        }
    }
    function replay() {
        getElementById("begin").style.display = 'none';
        getElementById('player').style.display = 'none';
        if(getElementById('result')!=null){
            getElementById('result').textContent = '';
        }
        getElementById('RAZ').style.display = 'none';
        getElementById('compteur').style.fontWeight = 'bold';
        getElementById('compteur').style.fontSize = '20px';
        getElementById('compteur').textContent = `Player ${player1} = ${victoirePlayer1} contre Player ${player2} = ${victoirePlayer2}`;
        initializeGame(passage++);
    }

    function querySelectorRow(rown){
        return querySelector(row,0);
    }

    function querySelectorCol(col){
        return querySelector(0,col);
    }

    function querySelector(row, col){
        return document.querySelector(`[data-row="${row}"][data-col="${col}"]`);
    }

    function getElementById(name){
        return document.getElementById(name);
    }

    function chooseBestColumnForWin() {
        let bestCol = [];
        const availableColumns = getAvailableColumns();

        for (const col of availableColumns) {
            const row = getLowestEmptyRow(col);
            const evaluate2 = checkForWin(row, col);
            if(evaluate2){
                bestCol.push([col,row]);
            }
        }
        return bestCol;
    }
    
    function checkForWin(row, col) {
        return (
            checkDirection(row, col, 0, 1) || // Horizontal
            checkDirection(row, col, 1, 0) || // Vertical
            checkDirection(row, col, 1, 1) || // Diagonal
            checkDirection(row, col, -1, 1)   // Diagonal
        );
    }

    function checkDirection(row, col, rowDir, colDir) {
        let count = 1;

        let pos = countInDirection(row, col, rowDir, colDir);
		count += pos;

        let neg = countInDirection(row, col, -rowDir, -colDir);
        count += neg;

        if(count>1){
            if(currentPlayer===player1){
                if(neg>pos){
                    colPlayer1=col-(-colDir);
                }else if(neg<pos){
                    colPlayer1=col-(colDir);
                }else{
                    colPlayer1=col;
                }
                if(colPlayer1===-1){
                    colPlayer1 = 0;
                }else if(colPlayer1>6){
                    colPlayer1 = 6;
                }
            }
        }
        return count >= 4;
    }

    function countInDirection(row, col, rowDir, colDir) {
        let count = 0;
        let currentRow = row + rowDir;
        let currentCol = col + colDir;
        while (isValidCell(currentRow, currentCol) && isCurrentPlayer(currentRow, currentCol)) {
            count++;
            currentRow += rowDir;
            currentCol += colDir;
        }
        return count;
    }

    function isValidCell(row, col) {
        return row >= 0 && row < rows && col >= 0 && col < cols;
    }

    function isCurrentPlayer(row, col) {
        const cell = querySelector(row,col);
		if(cell!=undefined){
			return cell.title === currentPlayer;
		}else{
			return false;
		}
    }

    function chooseBestColumn() {
        let scores = [];
        const availableColumns = getAvailableColumns();

        for (const col of availableColumns) {
            const row = getLowestEmptyRow(col);
            scores.push(evaluatePosition(row, col));
        }

        // Choisir la colonne avec le score le plus élevé
        let maxIndice = indiceMaxValues(scores);
        let bestColumnIndex;
        let evaluateOrig=-1;
        for(let index of maxIndice){
            const bestColumn = index;

            const row2 = getLowestEmptyRow(bestColumn);
            const evaluate2 = evaluatePosition(row2 - 1, bestColumn);

            if(evaluateOrig === -1){
                evaluateOrig = evaluate2;
                bestColumnIndex = bestColumn;
            }
            else if(evaluateOrig>evaluate2){
                evaluateOrig = evaluate2;
                bestColumnIndex = bestColumn;
            }

        }

        return availableColumns[bestColumnIndex];
    }

    // retourne toutes les colonnes disponibles
    function getAvailableColumns() {
        const availableColumns = [];
    
        for (let col = 0; col < cols; col++) {
            const topCell = getCell(0, col);
            if (topCell != undefined && !topCell.title) {
                availableColumns.push(col);
            }
        }
    
        return availableColumns;
    }

    function indiceMaxValues(tab){
        let max = Math.max(...tab);
        let indices = [];
        for(let i=0; i < tab.length; i++){
            if(tab[i] === max){
                indices.push(i);
            }
        }
        return indices;
    }
    function getCell(row, col) {
        return querySelector(row,col);
    }

    // retourne la toute dernière ligne de la colonne qui n'est pas alimentéé
    function getLowestEmptyRow(col) {
        for (let row = rows - 1; row >= 0; row--) {
            const cell = getCell(row, col);
            if (!cell.title) {
                return row;
            }
        }
        return -1; // Colonne pleine
    }

    function evaluatePosition(row, col) {
        // Logique d'évaluation simple
        // Vous pouvez ajuster cela pour une meilleure stratégie
        const horizont = evaluateDirection(row, col, 0, 1);
       
        const vertical = evaluateDirection(row, col, 1, 0);
       
        const diagonal1 = evaluateDirection(row, col, 1, 1);
       
        const diagonal2 = evaluateDirection(row, col, 1, -1);
       
        return horizont + // Horizontal
               vertical + // Vertical
               diagonal1+ // Diagonale /
               diagonal2; // Diagonale \
    }

    function evaluateDirection(row, col, rowIncrement, colIncrement) {
        let score = 0;

        // Évaluer la direction vers la droite
        score += evaluateLine(row, col, rowIncrement, colIncrement);

        // Évaluer la direction vers la gauche
        score += evaluateLine(row, col, -rowIncrement, -colIncrement);

        return score;
    }

    function evaluateLine(row, col, rowIncrement, colIncrement) {
        const line = [];
        let bonus = 0;
        let currentRow = row;
        let currentCol = col;
        let isDiagonal = false;

        if(rowIncrement!=0 && colIncrement!=0){
            isDiagonal = true;
            const addRow = row - rowIncrement;
            const addCol = col - colIncrement;
            if(addRow>=0 && addCol>=0){
                const cell = getCell(addRow, addCol);
                const cellInf = getCell(addRow+1, addCol);
                if((cell!=undefined && cell.title!='') && (cellInf!=undefined && cellInf.title!='')){
                    line.push(cell);
                }
            }
        }
        // Collecter les pièces dans la ligne
        while (currentRow >= 0 && currentRow < rows && currentCol >= 0 && currentCol < cols) {
            line.push(getCell(currentRow, currentCol));
            currentRow += rowIncrement;
            currentCol += colIncrement;
        }

        // Évaluer la ligne en attribuant des points en fonction des pièces
        const result = evaluateLineHelper(line, player1);

        if(isDiagonal){
            bonus = checkDiagonal(line);
        }

        return result+bonus;
        // return evaluateLineHelper(line, player2) - evaluateLineHelper(line, player1);
    }

    function evaluateLineHelper(line, player) {
        let suite = [];
        let playerCount = 0;
        let nbSuite = 0;

        if(line.length > 2 && line[0].title === '' && line[1].title === player1 && line[2].title === player1){
            playerCount+=300;
            nbSuite+=1;
        }
        if(line.length > 2 && line[0].title === '' && line[1].title === player1 && line[2].title === ''){
            playerCount+=100;
            nbSuite+=1;
        }

        for(let iCpt = 0; iCpt<line.length;iCpt++){
            if(line[iCpt].title===player1){
                suite.push(line[iCpt].title);
                playerCount++;
                nbSuite++;
                if(nbSuite ===2){
                    playerCount+=30;
                }
                if(nbSuite >2){
                    if(suite.length > 3){
                        let averSuite=Math.floor(suite.length/2);
                        let bonusPlayer = 300;
                        for(let iP = averSuite - 1; iP<=averSuite + 1;iP++){
                            if(suite[iP] === player2){
                                bonusPlayer = 0;
                                break;
                            }
                        }
                        playerCount+=bonusPlayer;
                    }else{
                        if(iCpt === line.length - 1){
                            if(line[0].title === ''){
                                playerCount+=300;
                            }
                        }else{
                            const iMin = iCpt-3<0?iCpt=0:iCpt-3;
                            const iMax = iCpt+1>line.length?iCpt=line.length:iCpt+1;
                            if(line[iMin].title === '' || line[iMax].title === ''){
                                playerCount+=300;
                            }
                        }
                    }
                }
            }else{
                if(nbSuite>0)
                    if(line[iCpt].title === player2){
                        suite.push(line[iCpt].title);
                        nbSuite-=3;
                    }else{
                        suite.push(line[iCpt].title);
                    }
            }
        }
        return playerCount;
	}

    function checkDiagonal(line){
        const play2 = '2';
        const play1 = '1';
        const blank = '0';
        let result = '';
        for(let iNbLine = 0; iNbLine < line.length ; iNbLine++){
            if(line[iNbLine].title === player2){
                result = result + play2;
            }else if(line[iNbLine].title === player1){
                result = result +  play1;
            }else if(line[iNbLine].title === ''){
                result = result + blank;
            }
        }

        return checkResult(result);
    }

    function checkResult(verifResult){
        let result = ['1110','0111','1011','1101'];
        for(let iR = 0; iR < result.length; iR++){
            if(verifResult.includes(result[iR])){
                return 1000;
            }
        }
        return 0;
    }

initializeGame(0);