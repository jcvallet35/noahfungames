var answer;
var bonnesReponses = 0;
var mauvaisesReponses = 0;
var tempsPasse = 0; // Temps écoulé en secondes
var chronoInterval;
var operator;

function generateQuestion(operator) {
	if(level == undefined)
		level = 1;
	var coeff;
	switch(operator){
		case '+':
			coeff = 10*level;
			break;
		case '-':
			coeff = 10*level;
			break;
		case '*':
			coeff = 10*level;
			break;
		case '/':
			coeff = 20*level;
			break;
	}
	
	var num1 = Math.floor(Math.random() * coeff) + 1; // Nombre aléatoire de 1 à 10
	var num2 = Math.floor(Math.random() * coeff) + 1; // Nombre aléatoire de 1 à 10
	
	if(operator==='-'){
		while(num1<num2){
			num2 = Math.floor(Math.random() * coeff) + 1; // Nombre aléatoire de 1 à 10
		}
	}
	
	switch (operator) {
		case '+':
			var sum = num1 + num2;
			return { num1: num1, num2: num2, answer: sum };
			break;
		case '-':
			var sum = num1 - num2;
			return { num1: num1, num2: num2, answer: sum };
			break;
		case '*':
			var sum = num1 * num2;
			return { num1: num1, num2: num2, answer: sum };
			break;
		case '/':
			let bVerif = false;
			while(num1 < 2){
				num1 = Math.floor(Math.random() * coeff) + 1;
			}
			while (!bVerif)
			{
				if ((num1 < num2 || num1 === num2) || num1 % num2 != 0)
				{
					num2 = Math.floor(Math.random() * coeff) + 1;
				}else {
					bVerif=true;
				}
			}
			var sum = num1 / num2;
			return { num1: num1, num2: num2, answer: sum };
			break;
	}
}

function displayQuestion(operator) {

	var question = generateQuestion(operator);
	
	document.getElementById('question').textContent = `Combien font : ${question.num1} ${operator} ${question.num2} ?`;
	document.getElementById('userAnswer').value = '';
	answer = question.answer;
	return question;
}

function checkAnswer() {
	if (event.keyCode === 13) {
		var userAnswer = parseInt(document.getElementById('userAnswer').value);
		
		if (userAnswer === answer) {
			document.getElementById('result').textContent = 'Correct !';
			document.getElementById('result').style.fontWeight = 'bold';
			document.getElementById('result').style.fontSize = '20px';
			document.getElementById('result').style.color = 'green';
			bonnesReponses++;
		} else {
			document.getElementById('result').textContent = 'Incorrect. La réponse correcte est ' + answer + '.';
			document.getElementById('result').style.fontWeight = 'bold';
			document.getElementById('result').style.fontSize = '20px';
			document.getElementById('result').style.color = 'red';
			mauvaisesReponses++;
		}
		var question = displayQuestion(getRandomOperator());
	}
	if ((bonnesReponses + mauvaisesReponses) === 60) {
		if (bonnesReponses > 56) {
			var player_video = document.getElementById('player');
			player_video.style.display = 'block';
			loadRandomVideo();
		}

		clearInterval(chronoInterval);
		document.getElementById('RAZ').style.display = 'block';
		document.getElementById('userAnswer').style.display = 'none';
		document.getElementById('question').style.display = 'none';
		document.getElementById('result').style.color = 'green';
		document.getElementById('result').style.fontSize = '24px';
		

		setInterval(blink, 500);
		
		document.getElementById('result').style.fontWeight = 'bold';
		document.getElementById('result').textContent = 'Vous avez ' + bonnesReponses + ' bonnes réponses sur ' + (bonnesReponses + mauvaisesReponses) + ' en ' + tempsPasse + ' secondes';
	}
	
	
}

function blink() {
	if (document.getElementById('result').style.color === 'green') {
		document.getElementById('result').style.color = ''; // Rétablir la couleur par défaut (noire ou autre)
	} else {
		document.getElementById('result').style.color = 'green'; // Changer la couleur du texte en rouge
	}
}

function init() {
	operator = getRandomOperator();
	// Afficher la première question au chargement de la page
	displayQuestion(operator);
	document.getElementById('begin').style.display = 'none';
	document.getElementById('chrono').style.display = 'block';
	replay();
}

function replay() {
	var answer = 0;
	bonnesReponses = 0;
	mauvaisesReponses = 0;
	if(level == undefined)
		level = 1;
	else
		level = level+1;
	document.getElementById('player').style.display = 'none';
	document.getElementById('RAZ').style.display = 'none';
	document.getElementById('result').textContent = '';
	document.getElementById('userAnswer').style.display = 'block';
	document.getElementById('question').style.display = 'block';
	clearInterval(chronoInterval);
	tempsPasse = 0; // Temps écoulé en secondes
	chronoInterval = 0;
	document.getElementById('tempsPasse').textContent = tempsPasse; 
	demarrerChrono();
}

function demarrerChrono() {
	chronoInterval = setInterval(function() {
		tempsPasse++;
		document.getElementById('tempsPasse').textContent = tempsPasse;
	}, 1000); // Mettre à jour le chronomètre toutes les 1000 ms (1 seconde)
}

function loadRandomVideo() {
	// Array de vidéos YouTube d'environ 1 minute (ajoutez vos liens ici)
	/*
		'v=yws-sP342Ik?si=Y-4W0-121f6XBhTx',
		'v=DqNaPewELqc?si=DPdzL-3nvhEl_3_j',
		'v=DZXWZq-MfSU?si=pDb4CUPzm57hikI9',
		'v=L3qJFjlDvKY?t=35?si=CSx1xDAjsAnEZiID',
		'v=9B_e_XJyHmU?si=gwPuTVH2YZJgFBfb',
		'v=5Q6n5NE6Cb4?si=AVSrr9Dzc4ie0h8c',
		'v=cR7o655tVw8?si=unt45V1XYwhj8PIc',
		'v=32n1ld2KSdA?si=tLeaoWmLoyysM1ML',
		'v=geLsdbzDwGE?si=9vvwM2dZ7_9fgwZX',*/
	switch(localStorage.getItem('profil')){
		case '1':
		default:
			var videoLinks = [
				'v=sDMLDhaxjyI?si=ea_xiak6y85fEvh3',
				'v=3UEnsVfM2dA?si=D3c7kHIFHDx4xSRq'
			];

			// Choisir un lien vidéo aléatoire
			var randomIndex = Math.floor(Math.random() * videoLinks.length);
			var randomVideoLink = videoLinks[randomIndex];

			// Extraire l'ID de la vidéo depuis le lien
			var videoId = randomVideoLink.split('v=')[1];

			// Construire l'URL de l'iframe
			var iframeUrl = 'https://www.youtube.com/embed/' + videoId + '?apikey=AIzaSyDc9EV4pA80VOONaSp9NsGWONSEpdktIHo&enablejsapi=1&autoplay=1';
			
			// Créer l'élément iframe pour la vidé
			var playerDiv = document.getElementById('player');
			playerDiv.innerHTML = '<iframe width="560" height="315" src="' + iframeUrl + '" frameborder="0" allowfullscreen></iframe>';
			break;
		case '2':
			var videoLinks = [
				'v=1NhppWbuwbB6W1FIrApPOC72gGAKnoL2Q'
			];
			
			var randomIndex = Math.floor(Math.random() * videoLinks.length);
			var randomVideoLink = videoLinks[randomIndex];

			// Extraire l'ID de la vidéo depuis le lien
			var videoId = randomVideoLink.split('v=')[1];
			var iframeUrl = 'https://drive.google.com/file/d/' + videoId + '/preview';

			var playerDiv = document.getElementById('player');
			playerDiv.innerHTML = '<iframe src="' + iframeUrl + '" allow="autoplay"></iframe>';
			break;
		}
}


function getRandomOperator() {
	const randomNumber = Math.random();
	if(randomNumber < 0.25) {
		return '+';
	}else if(randomNumber < 0.50) {
		return '-';
	}else if(randomNumber < 0.75) {
		return '/';
	}else {
		return '*';
	}
}

// Appelez demarrerChrono() au chargement de la page pour démarrer le chronomètre
window.onload = demarrerChrono;
