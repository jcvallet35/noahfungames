const questions = [
	{
	question: "Je me réveille",
	correctAnswer: "I wake up"
	},
	
	{
	question: "Je me lève (du lit)",
	correctAnswer: "I get up"
	},
	
	{
	question: "Je prends une douche",
	correctAnswer: "I get a shower"
	},
	
	{
	question: "Je me brosse les dents",
	correctAnswer: "I brush my teeth"
	},
	
	{
	question: "Je me brosse les cheveux",
	correctAnswer: "I brush my hair"
	},
	
	{
	question: "Je m'habille",
	correctAnswer: "I get dressed"
	},
	
	{
	question: "Je fais mon lit",
	correctAnswer: "I make the bed"
	},
	
	{
	question: "Je prends mon déjeuner",
	correctAnswer: "I have breakfast"
	},
	
	{
	question: "Je vais à l'école",
	correctAnswer: "I go to school"
	},
	
	{
	question: "Je lis",
	correctAnswer: "I read"
	},
	
	{
	question: "Je mange (repas du midi / soir)",
	correctAnswer: "I have lunch"
	},
	
	{
	question: "J'écris",
	correctAnswer: "I write"
	},
	
	{
	question: "Je rentre à la maison",
	correctAnswer: "I come home"
	},
	
	{
	question: "Je fais mes devoirs",
	correctAnswer: "I do my homework"
	},
	
	{
	question: "Je fais du skateboard",
	correctAnswer: "I go skateboard"
	},
	
	{
	question: "Je joue avec mes amis",
	correctAnswer: "I play with my friends"
	},
	
	{
	question: "Je regarde la TV",
	correctAnswer: "I watch tv"
	},
	
	{
	question: "Je navigue sur internet",
	correctAnswer: "I surf the net"
	},
	
	{
	question: "Je mange",
	correctAnswer: "I eat"
	},
	
	{
	question: "Je fais la vaisselle",
	correctAnswer: "I wash the dishes"
	},
	
	{
	question: "Je me déshabille",
	correctAnswer: "I get undressed"
	},
	
	{
	question: "Je vais me coucher",
	correctAnswer: "I go to sleep"
	},
	
	{
	question: "toujours",
	correctAnswer: "always"
	},
	
	{
	question: "habituellement",
	correctAnswer: "usually"
	},
	
	{
	question: "souvent",
	correctAnswer: "often"
	},
	
	{
	question: "parfois",
	correctAnswer: "sometimes"
	},
	
	{
	question: "rarement",
	correctAnswer: "rarely"
	},
	
	{
	question: "jamais",
	correctAnswer: "never"
	}
	];