var chronoInterval;
var cpt=0;
var cardValues;

function createCard(){
	cardValues = [
		'A', 'A', 'B','B','C','C','D','D','E','E','F','F','G','G','H','H','J','J','K','K','L','L'
	];

<!-- ,'I','I','J','J','K','K','L','L','M','M','N','O','P','P','Q','Q','R','R','S','S','T','T','U','U','V','V','W','W','X','X','Y','Y','Z', -->


	// Mélanger les cartes
	shuffleArray(cardValues);
							
	const gameContainer = document.getElementById('game-container');
	loadRandomVideo();
	// Créer les cartes
	cardValues.forEach((value, index) => {
		const card = document.createElement('div');
		card.classList.add('card');
		card.dataset.value = value;

		const cardInner = document.createElement('div');
		cardInner.classList.add('card-inner');

		const frontFace = document.createElement('div');
		frontFace.classList.add('face', 'front');

		const backFace = document.createElement('div');
		backFace.classList.add('face', 'back');
		backFace.style = "background-image: url('./img/cards/" + value + ".png')";
		backFace.textContent = value;

		cardInner.appendChild(frontFace);
		cardInner.appendChild(backFace);
		card.appendChild(cardInner);

		gameContainer.appendChild(card);
	});
}

const resetButton = document.getElementById('RAZ');
	resetButton.addEventListener('click', () => {
		removeAllCards();
		createCard();
		loadClick();
		replay();
});

function loadClick(){
	// Gérer les clics sur les cartes
	const cards = document.querySelectorAll('.card');
	let activeCards = [];

	cards.forEach(card => {
		card.addEventListener('click', () => {
			if (!card.classList.contains('active')) {
				card.classList.add('active');
				activeCards.push(card);

				if (activeCards.length === 2) {
					const [card1, card2] = activeCards;
					const value1 = card1.getAttribute('data-value');
					const value2 = card2.getAttribute('data-value');
					
					if (value1 === value2) {
						cpt = cpt+2;
						if(cpt === cardValues.length){
							clearInterval(chronoInterval);
							document.getElementById('result').style.fontWeight = 'bold';
							document.getElementById('result').textContent = 'Vous avez résolu le jeu de mémorisation en ' + tempsPasse + ' secondes';
							document.getElementById('RAZ').style.display = 'block';
							var player_video = document.getElementById('player');
							player_video.style.display = 'block';
							loadRandomVideo();
						}
						// Paires identiques, supprimez-les
						/*setTimeout(() => {
							card1.style.display = 'none';
							card2.style.display = 'none';
						}, 1000);*/
					} else {
						// Cartes différentes, retournez-les
						setTimeout(() => {
							card1.classList.remove('active');
							card2.classList.remove('active');
						}, 1000);
					}
					activeCards = [];
				}
			}
		});
	});
}

function shuffleArray(array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
}

function removeAllCards(){
	const gameContainer = document.getElementById('game-container');
	while(gameContainer.firstChild){
		gameContainer.removeChild(gameContainer.firstChild);
	}
}

function init() {
	createCard();
	loadClick();
	document.getElementById('begin').style.display = 'none';
	document.getElementById('chrono').style.display = 'block';
	replay();
}

function replay() {
	cpt = 0;
	document.getElementById('result').textContent = '';
	document.getElementById('RAZ').style.display = 'none';
	clearInterval(chronoInterval);
	tempsPasse = 0; // Temps écoulé en secondes
	chronoInterval = 0;
	document.getElementById('tempsPasse').textContent = tempsPasse; 
	demarrerChrono();
}
	
function demarrerChrono() {
	chronoInterval = setInterval(function() {
		tempsPasse++;
		document.getElementById('tempsPasse').textContent = tempsPasse;
	}, 1000); // Mettre à jour le chronomètre toutes les 1000 ms (1 seconde)
}

function loadRandomVideo() {
	// Array de vidéos YouTube d'environ 1 minute (ajoutez vos liens ici)
	var videoLinks = [
		'v=jWLugcvnxsM?si=64zf17Gm3lY4lQE0C',
		'v=is8Si8eG2TA?si=BUBJOxJMf7JjE4yD'
	];

	// Choisir un lien vidéo aléatoire
	var randomIndex = Math.floor(Math.random() * videoLinks.length);
	var randomVideoLink = videoLinks[randomIndex];

	// Extraire l'ID de la vidéo depuis le lien
	var videoId = randomVideoLink.split('v=')[1];

	// Construire l'URL de l'iframe
	var iframeUrl = 'https://www.youtube.com/embed/' + videoId + '?apikey=AIzaSyDc9EV4pA80VOONaSp9NsGWONSEpdktIHo&enablejsapi=1&autoplay=1';

	// Créer l'élément iframe pour la vidéo
	var playerDiv = document.getElementById('player');
	playerDiv.innerHTML = '<iframe width="560" height="315" src="' + iframeUrl + '" frameborder="0" allowfullscreen></iframe>';
}